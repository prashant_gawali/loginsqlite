package com.example.loginsqlite;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;

public class LoginActivity extends AppCompatActivity {
    EditText username1, password1;
    Button register1, logIn1;
    DBHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        username1 = findViewById(R.id.username1);
        password1 = findViewById(R.id.password1);
        logIn1 = findViewById(R.id.login1);
        register1 = findViewById(R.id.register1);
        db = new DBHelper(this);

        Toast.makeText(this, "This is Login Activity", Toast.LENGTH_SHORT).show();

        register1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gotoRegister = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(gotoRegister);
            }
        });
        logIn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user1 = username1.getText().toString();
                String pass1 = password1.getText().toString();

                if (user1.equals("") || pass1.equals("")) {
                    Snackbar.make(v, "All Fields are Mandatory", Snackbar.LENGTH_LONG).show();
                } else {
                    Boolean checkuserpass = db.checkusernamepassword(user1, pass1);
                    if (checkuserpass == true) {
                        Toast.makeText(LoginActivity.this, "Log-in Successful", Toast.LENGTH_SHORT).show();
                        Intent gotoHome = new Intent(LoginActivity.this, HomeActivity.class);
                        startActivity(gotoHome);
                    } else {
                        Snackbar.make(v, "INVALID CREDENTIALS", Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        });
    }
}