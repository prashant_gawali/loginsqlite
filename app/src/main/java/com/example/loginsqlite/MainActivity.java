package com.example.loginsqlite;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity {
    EditText username, password, repassword;
    Button register, logIn;
    private DBHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toast.makeText(this, "Main Activity", Toast.LENGTH_SHORT).show();

        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        repassword = findViewById(R.id.repassword);
        register = findViewById(R.id.register);
        logIn = findViewById(R.id.logIn);
        db = new DBHelper(this);

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String user = username.getText().toString();
                String pass = password.getText().toString();
                String repass = repassword.getText().toString();
                if (user.equals("") || (pass.equals("")) || (repass.equals(""))) {
                    Snackbar.make(v, "All Fields are Mandatory", BaseTransientBottomBar.LENGTH_LONG).show();
                } else {
                    if (pass.equals(repass)) {
                        Boolean checkuser = db.checkusername(user);
                        if (checkuser == false) {
                            Boolean insert = db.insertData(user, pass);
                            if (insert == true) {
                                Toast.makeText(MainActivity.this, "Registered Successful", Toast.LENGTH_SHORT).show();
                                Intent goLogin = new Intent(MainActivity.this, LoginActivity.class);
                                startActivity(goLogin);

                            } else {
                                Toast.makeText(MainActivity.this, "Registration Failed!!", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(MainActivity.this, "Username Already Exists! Goto Login Page", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Snackbar.make(v, "Password didn't Match..Re-Type Passwords", Snackbar.LENGTH_LONG).show();
                    }
                }

            }
        });

        logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goLogin = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(goLogin);
            }
        });
    }
}